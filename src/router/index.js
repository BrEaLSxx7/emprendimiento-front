import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/homeComponent';
import Ser from '@/components/serComponent';
import Empresa from '@/components/empresaComponent';
import Acerca from '@/components/acercaComponent';
import Unidad from '@/components/unidadComponent';
import Victimas from '@/components/victimasComponent';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/ser',
      name: 'Ser',
      component: Ser
    },
    {
      path: '/empresa',
      name: 'Empresa',
      component: Empresa
    },
    {
      path: '/acerca',
      name: 'Acerca',
      component: Acerca
    },
    {
      path: '/unidad',
      name: 'Unidad',
      component: Unidad
    },
    {
      path: '/victimas',
      name: 'Victimas',
      component: Victimas
    },
    {
      path: '**',
      redirect: '/',
      component: Home
    }
  ]
});
